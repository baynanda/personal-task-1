<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PendataanMahasiswa extends Controller
{
    public function index()
    {
        return view('pendataan-mahasiswa');
    }
    public function store(Request $request)
    {
        $request->validate([
            'filePasFoto' => 'required|mimes:jpg,jpeg,png|max:2048',
            'fileSertifikat' => 'mimes:zip,rar|max:10240',
            'fileCV' => 'mimes:pdf|max:10240'
        ]);
    
        $fileName = time().'.'.$request->filePasFoto->extension();  
     
        $request->filePasFoto->move(public_path('uploads'), $fileName);
     
        return back()
            ->with('success','You have successfully upload file.')
            ->with('file', $fileName);
   
    }
}
