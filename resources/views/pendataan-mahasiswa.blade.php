<!DOCTYPE html>
<html>

<head>
    <title>Pendataan Mahasiswa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="row text-black" style="height: 100vh;">
        <div class="col-5 p-4">
            <h2 class="container">Pendataan Mahasiswa</h2>
            <div class="mb-3">
                <form action="{{ route('submit') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="container py-1">
                        <label class="form-label" for="inputName">Nama Lengkap</label>
                        <input type="text" name="name" id="inputName" class="form-control" placeholder="nama">
                    </div>
                    <div class="container py-1">
                        <label class="form-label" for="inputGendere">Jenis Kelamin</label><br>
                        <div class="btn-group" role="group" aria-label="Basic checkbox toggle button group">
                            <input type="radio" name="gender" class="btn-check" id="btncheck1" autocomplete="off">
                            <label class="btn btn-outline-primary" for="btncheck1">Laki-laki</label>

                            <input type="radio" name="gender" class="btn-check" id="btncheck2" autocomplete="off">
                            <label class="btn btn-outline-primary" for="btncheck2">Perempuan</label>
                        </div>
                    </div>
                    <div class="container py-1">
                        <label class="form-label" for="inputTempatLahir">Tempat Lahir</label>
                        <input type="text" name="tempatLahir" id="inputTempatLahir" class="form-control"
                            placeholder="kota">
                    </div>

                    <div class="container py-1">
                        <label class="form-label" for="date">Tanggal Lahir</label>
                        <input type="date" class="form-control" id="date">
                    </div>

                    <div class="container py-1">
                        <label class="form-label" for="inputPasFoto">Pas Foto (jpg, jpeg, png)</label>
                        <input type="file" name="filePasFoto" id="inputPasFoto"
                            class="form-control @error('file') is-invalid @enderror">
                    </div>

                    <div class="container py-1">
                        <label class="form-label" for="inputSertifikat">Sertifikat (zip, rar) - optional</label>
                        <input type="file" name="fileSertifikat" id="inputSertifikat"
                            class="form-control @error('file') is-invalid @enderror" placeholder="zip or rar">
                    </div>

                    <div class="container py-1">
                        <label class="form-label" for="inputCV">CV (pdf) - optional</label>
                        <input type="file" name="fileCV" id="inputCV"
                            class="form-control @error('file') is-invalid @enderror">
                    </div>

                    <div class="row container justify-content-start py-2">
                        <div class="col-2"><button type="submit" class="btn btn-success">Submit</button></div>
                        <div class="col-2"><button type="reset" class="btn btn-danger">Reset</button></div>
                    </div>

                    <div class="container">
                        @error('filePasFoto')
                        <span class="text-danger">{{ $message }}</span><br>
                        @enderror
                        @error('fileSertifikat')
                        <span class="text-danger">{{ $message }}</span><br>
                        @enderror
                        @error('fileCV')
                        <span class="text-danger">{{ $message }}</span><br>
                        @enderror
                    </div>
                </form>
            </div>

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <strong>{{ $message }}</strong>
            </div>
            @endif
        </div>
        <div class="col-7 d-flex align-items-center justify-content-end p-5 text-white"
            style="background-color:#2a6dba;">
            <h1>BINUS UNIVERSITY</h1>
        </div>
</body>

</html>
